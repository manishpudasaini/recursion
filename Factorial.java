package FabonacciSeries;

import java.util.Scanner;

public class Factorial {
    public Integer factorial(int a){
        if(a==0 && a==1){
            return 1;
        }
        else{
            return a*factorial(a-1);
        }

    }

    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the value of a::");
        int a = sc.nextInt();
        Factorial fc = new Factorial();
        fc.factorial(a);
    }


}
